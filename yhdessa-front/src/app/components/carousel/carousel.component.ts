import {Component, Input, OnInit} from '@angular/core';
import {NgbCarouselConfig} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent implements OnInit {

  @Input() hide = false;

  images = [1, 2, 3].map(() => `https://picsum.photos/1600/300?random&t=${Math.random()}`);

  constructor(carouselConfig: NgbCarouselConfig) {
    carouselConfig.interval = 10000;
    carouselConfig.wrap = true;
    carouselConfig.keyboard = false;
    carouselConfig.pauseOnHover = true;
  }

  ngOnInit() {
  }

}
