import { Component, OnInit } from '@angular/core';
import {AnnouncementType} from '../../models/announcementType';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss']
})
export class EventsComponent implements OnInit {

  events = AnnouncementType.Events;

  constructor() { }

  ngOnInit() {
  }

}
