import {Component, Injectable, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgbActiveModal, NgbDate, NgbDatepickerI18n, NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import {Announcement} from '../../models/announcement';

const I18N_VALUES = {
  'fi-FI': {
    weekdays: ['Ma', 'Ti', 'Ke', 'To', 'Pe', 'La', 'Su'],
    months: ['Tammi', 'Helmi', 'Maalis', 'Huhti', 'Touko', 'Kesä', 'Heinä', 'Elo', 'Syys', 'Loka', 'Marras', 'Joulu'],
  }
};

@Injectable()
export class I18n {
  language = 'fi-FI';
}

@Injectable()
export class CustomDatepickerI18n extends NgbDatepickerI18n {

  constructor(private i18n: I18n) {
    super();
  }

  getWeekdayShortName(weekday: number): string {
    return I18N_VALUES[this.i18n.language].weekdays[weekday - 1];
  }
  getMonthShortName(month: number): string {
    return I18N_VALUES[this.i18n.language].months[month - 1];
  }
  getMonthFullName(month: number): string {
    return this.getMonthShortName(month);
  }

  getDayAriaLabel(date: NgbDateStruct): string {
    return `${date.day}-${date.month}-${date.year}`;
  }
}

@Component({
  selector: 'app-announcement-modal',
  templateUrl: './announcement-modal.component.html',
  styleUrls: ['./announcement-modal.component.scss'],
  providers: [I18n, {provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n}]
})
export class AnnouncementModalComponent implements OnInit {

  @Input() selectedAnnouncement: Announcement;
  form: FormGroup;
  submitted = false;
  times: string[] = [];

  constructor(public activeModal: NgbActiveModal, private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.generateTimes();

    this.form = this.formBuilder.group({
      title: ['', [Validators.required, Validators.maxLength(50)]],
      message: ['', [Validators.required, Validators.maxLength(1000)]],
      type: [1, Validators.required],
      startDate: [undefined],
      endDate: [undefined],
      startTime: [undefined],
      endTime: [undefined],
      accept: [false],
      private: [false],
      invite: [false]
    }, {
      validator: this.dateLessThan('startDate', 'endDate', 'startTime', 'endTime')
    });
  }

  dateLessThan(fromDate: string, toDate: string, fromTime: string, toTime: string) {
    return (formGroup: FormGroup) => {
      const fromDateControl = formGroup.controls[fromDate];
      const toDateControl = formGroup.controls[toDate];
      const fromTimeControl = formGroup.controls[fromTime];
      const toTimeControl = formGroup.controls[toTime];

      if (!fromDateControl.value || !fromTimeControl.value || !toDateControl.value || !toTimeControl.value || (toDateControl.errors && !toDateControl.errors.endBefore) || (toTimeControl.errors && !toTimeControl.errors.endBefore)) {
        return;
      }

      const startDate: NgbDate = fromDateControl.value;
      const endDate: NgbDate = toDateControl.value;
      const startDateTime = new Date(startDate.year + '-' + startDate.month + '-' + startDate.day + ' ' + fromTimeControl.value);
      const endDateTime = new Date(endDate.year + '-' + endDate.month + '-' + endDate.day + ' ' + toTimeControl.value);

      if (endDateTime < startDateTime) {
        toDateControl.setErrors({ endBefore: true });
        toTimeControl.setErrors({ endBefore: true });
      } else {
        toDateControl.setErrors(null);
        toTimeControl.setErrors(null);
      }
    };
  }

  get f() { return this.form.controls; }

  save() {
    this.submitted = true;

    if (this.form.invalid) {
      return;
    }

    this.selectedAnnouncement.title = this.f.title.value;
    this.selectedAnnouncement.message = this.f.message.value;
    this.selectedAnnouncement.type = this.f.type.value;

    const startDate: NgbDate = this.f.startDate.value;
    const endDate: NgbDate = this.f.endDate.value;
    const startDateTime = new Date(startDate.year + '-' + startDate.month + '-' + startDate.day + ' ' + this.f.startTime.value);
    const endDateTime = new Date(endDate.year + '-' + endDate.month + '-' + endDate.day + ' ' + this.f.endTime.value);

    this.selectedAnnouncement.startDateTime = startDateTime;
    this.selectedAnnouncement.endDateTime = endDateTime;
    this.selectedAnnouncement.accept = this.f.accept.value;
    this.selectedAnnouncement.invite = this.f.invite.value;
    this.selectedAnnouncement.private = this.f.private.value;

    this.activeModal.close(this.selectedAnnouncement);
  }

  close() {
    this.activeModal.dismiss();
  }

  limitMessage() {
    if (!this.f.message.value || this.f.message.value.length <= 1000) {
      return;
    }

    this.f.message.setValue(this.f.message.value.toString().substring(0, 1000));
  }

  limitTitle() {
    if (!this.f.title.value || this.f.title.value.length <= 50) {
      return;
    }

    this.f.title.setValue(this.f.title.value.toString().substring(0, 50));
  }

  private generateTimes() {
    for (let hour = 0; hour <= 24; hour++) {
      for (let minutes = 0; minutes < 60; minutes += 30) {
        this.times.push(this.padLeft(hour.toString()) + ':' + this.padRight(minutes.toString()));
      }
    }
  }

  private padLeft(s: string) {
    if (s.length === 1) {
      return '0' + s;
    }

    return s;
  }

  private padRight(s: string) {
    if (s.length === 1) {
      return s + '0';
    }

    return s;
  }
}
