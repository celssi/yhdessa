import {Component, OnDestroy, OnInit} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import {UserService} from '../../services/user.service';
import {AnnouncementModalComponent} from '../announcement-modal/announcement-modal.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Announcement} from '../../models/announcement';
import {AnnouncementService} from '../../services/announcement.service';
import {ToastrService} from 'ngx-toastr';
import {MessageService} from '../../services/message.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
  fullName = '';
  isLoggedIn = false;
  navbarOpen = false;
  unreadMessagesCount = 0;

  private newMessagesSubscription: Subscription;

  constructor(private usersService: UserService,
              private announcementService: AnnouncementService,
              private router: Router,
              private modalService: NgbModal,
              private toastr: ToastrService,
              private messageService: MessageService) {
    router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        this.isLoggedIn = usersService.isLoggedIn();
        this.fullName = usersService.getFullName();
      }
    });
  }

  ngOnInit(): void {
    this.newMessagesSubscription = this.messageService.newMessages.subscribe((count: number) => {
      this.unreadMessagesCount = count;
    });
  }

  ngOnDestroy(): void {
    this.newMessagesSubscription.unsubscribe();
  }

  toggleNavbar() {
    this.navbarOpen = !this.navbarOpen;
  }

  closeNavbar() {
    this.navbarOpen = false;
  }

  logout() {
    this.navbarOpen = false;
    this.usersService.logout();
    this.router.navigate(['/kirjaudu']);
  }

  createNew() {
    const modalReference = this.modalService.open(AnnouncementModalComponent,  { size: 'lg' });
    modalReference.componentInstance.selectedAnnouncement = new Announcement();

    modalReference.result.then((announcement: Announcement) => {
      this.announcementService.create(announcement).subscribe((response: Announcement) => {

        this.toastr.success('Ilmoitus luotu onnistuneesti!', '', {
          positionClass: 'toast-top-center',
          timeOut: 2000
        });

        this.router.navigate(['/ilmoitus', response.identifier]);
      }, () => {
        this.toastr.error('Ilmoituksen luonti epäonnistui. Yritä hetken kuluttua uudelleen.', 'Virhe!', {
          positionClass: 'toast-top-center',
          timeOut: 3000
        });
      });
    }, () => {
    });
  }
}
