import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';
import {UserService} from '../../services/user.service';
import {ToastrService} from 'ngx-toastr';
import {User} from '../../models/user';

// tslint:disable:max-line-length
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  loading = false;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private spinner: NgxSpinnerService,
    private userService: UserService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      username: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(15)]],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      phone: ['', Validators.pattern('^(\\+\\d{1,3}[- ]?)?\\d{10}$')],
      password: ['', [Validators.required, Validators.minLength(8)]],
      passwordAgain: ['', Validators.required]
    }, {
      validator: this.mustMatch('password', 'passwordAgain')
    });
  }

  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.spinner.show();
    this.submitted = true;

    if (this.registerForm.invalid) {
      this.spinner.hide();
      return;
    }

    this.loading = true;

    const user: User = new User();
    user.username = this.f.username.value;
    user.firstName = this.f.firstName.value;
    user.lastName = this.f.lastName.value;
    user.email = this.f.email.value;
    user.phone = this.f.phone.value;
    user.password = this.f.password.value;

    this.userService.register(user).subscribe((result: any) => {
      this.router.navigate(['/kirjaudu']).then(() => {
        this.toastr.success('Rekisteröityminen onnistui!', '', {
          positionClass: 'toast-top-center',
          timeOut: 2000
        });
      });
    }, () => {
      this.toastr.error('Tarkista tiedot!', 'Rekisteröityminen epäonnistui!', {
        positionClass: 'toast-top-center',
        timeOut: 3000
      });

      this.loading = false;
      this.spinner.hide();
    }, ()  => {
      this.spinner.hide();
    });
  }

  mustMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];

      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
        return;
      }

      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ mustMatch: true });
      } else {
        matchingControl.setErrors(null);
      }
    };
  }

  checkIfUsernameExists() {
    if (this.f.username.errors && !this.f.username.errors.exists) {
      return;
    }

    const username = this.f.username.value;
    this.userService.exists(username).subscribe((exists: boolean) => {
      if (exists) {
        this.f.username.setErrors({ exists: true });
      } else {
        this.f.username.setErrors(null);
      }
    });
  }
}
