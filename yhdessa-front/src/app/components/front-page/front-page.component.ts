import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {UserService} from '../../services/user.service';
import {NavigationEnd, Router} from '@angular/router';
import {AnnouncementType} from '../../models/announcementType';

@Component({
  selector: 'app-front-page',
  templateUrl: './front-page.component.html',
  styleUrls: ['./front-page.component.scss']
})
export class FrontPageComponent implements OnInit {

  isLoggedIn = false;
  own = AnnouncementType.Own;
  events = AnnouncementType.Events;
  offered = AnnouncementType.Offered;
  needed = AnnouncementType.Needed;

  constructor(private usersService: UserService, private router: Router) {
    router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        this.isLoggedIn = usersService.isLoggedIn();
      }
    });
  }

  ngOnInit() {
  }

}
