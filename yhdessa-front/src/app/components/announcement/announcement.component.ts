import {Component, OnInit} from '@angular/core';
import {Announcement} from '../../models/announcement';
import {AnnouncementService} from '../../services/announcement.service';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {switchMap} from 'rxjs/operators';
import {Location} from '@angular/common';
import {User} from '../../models/user';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-announcement',
  templateUrl: './announcement.component.html',
  styleUrls: ['./announcement.component.scss']
})
export class AnnouncementComponent implements OnInit {

  currentUser: User;
  announcement: Announcement;
  attendees: User[] = [];

  constructor(private announcementService: AnnouncementService, private route: ActivatedRoute, private location: Location, private userService: UserService) { }

  ngOnInit() {
    this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
        this.announcementService.getOne(params.get('id')))
    ).subscribe((announcement: Announcement) => {
      this.announcement = announcement;

      this.userService.getCurrentUser().subscribe((user: User) => {
        this.currentUser = user;
      });

      this.announcementService.getAttendees(this.announcement.identifier).subscribe((attendees: User[]) => {
        this.attendees = attendees;
      });
    });
  }

  goBack() {
    this.location.back();
  }

  attend() {
    this.announcementService.attend(this.announcement.identifier).subscribe(() => {
      this.announcementService.getAttendees(this.announcement.identifier).subscribe((attendees: User[]) => {
        this.attendees = attendees;
      });
    });
  }

  unattend() {
    this.announcementService.unattend(this.announcement.identifier).subscribe(() => {
      this.announcementService.getAttendees(this.announcement.identifier).subscribe((attendees: User[]) => {
        this.attendees = attendees;
      });
    });
  }

  attended() {
    if (!this.currentUser) {
      return false;
    }

    let attending = false;
    this.attendees.forEach((user: User) => {
      if (user.id === this.currentUser.id) {
        attending = true;
      }
    });

    return attending;
  }
}
