import { Component, OnInit } from '@angular/core';
import {AnnouncementType} from '../../models/announcementType';

@Component({
  selector: 'app-own',
  templateUrl: './own.component.html',
  styleUrls: ['./own.component.scss']
})
export class OwnComponent implements OnInit {

  own = AnnouncementType.Own;

  constructor() { }

  ngOnInit() {
  }

}
