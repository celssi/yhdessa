import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-friend-request-modal',
  templateUrl: './friend-request-modal.component.html',
  styleUrls: ['./friend-request-modal.component.scss']
})
export class FriendRequestModalComponent implements OnInit {

  form: FormGroup;
  submitted = false;

  constructor(public activeModal: NgbActiveModal, private formBuilder: FormBuilder, private userService: UserService) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      username: ['', [Validators.required]],
    });
  }

  get f() { return this.form.controls; }

  save() {
    this.submitted = true;

    if (this.form.invalid) {
      return;
    }

    this.activeModal.close(this.f.username.value);
  }

  close() {
    this.activeModal.dismiss();
  }

  checkIfUsernameExists() {
    if (this.f.username.errors && !this.f.username.errors.exists) {
      return;
    }

    const username = this.f.username.value;
    this.userService.exists(username).subscribe((exists: boolean) => {
      if (!exists) {
        this.f.username.setErrors({ doesNotExist: true });
      } else {
        this.f.username.setErrors(null);
      }
    });
  }
}
