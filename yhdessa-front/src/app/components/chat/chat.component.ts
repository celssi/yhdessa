import {
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild
} from '@angular/core';
import {Message} from '../../models/message';
import {User} from '../../models/user';
import {UserService} from '../../services/user.service';
import {FriendshipService} from '../../services/friendship.service';
import {MessageService} from '../../services/message.service';
import {Subscription} from 'rxjs';
import {ScrollEvent} from 'ngx-scroll-event';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ToastrService} from 'ngx-toastr';
import {FriendRequestModalComponent} from '../friend-request-modal/friend-request-modal.component';
import {FriendshipRequest} from '../../models/friendshipRequest';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit, OnDestroy {
  allMessages: Message[] = [];
  messages: Message[] = [];
  currentUser: User;
  selectedRecipient: User;
  friendshipRequests: FriendshipRequest[] = [];
  allRecipients: User[] = [];
  recipients: User[] = [];
  writtenMessage = '';
  searchPhrase = '';

  private newMessagesSubscription: Subscription;
  private timerSubscription: Subscription;
  private autoScrollEnabled = true;

  @ViewChild('scrollMe', {static: false}) private scroll: ElementRef;

  constructor(private userService: UserService, private friendshipService: FriendshipService, private messageService: MessageService, private modalService: NgbModal, private toastr: ToastrService) { }

  ngOnInit() {
    this.userService.getCurrentUser().subscribe((currentUser: User) => {
      this.currentUser = currentUser;

      this.getFriends();
      this.getFriendshipRequests();
    });
  }

  public ngOnDestroy(): void {
    if (this.timerSubscription) {
      this.timerSubscription.unsubscribe();
    }

    if (this.newMessagesSubscription) {
      this.newMessagesSubscription.unsubscribe();
    }
  }

  sendMessage() {
    if (this.writtenMessage.length === 0) {
      return;
    }

    const message = new Message();
    message.content = this.writtenMessage;

    this.messageService.sendMessage(this.selectedRecipient, message).subscribe(() => {
      this.fetchMessages();
    });

    this.autoScrollEnabled = true;
    this.writtenMessage = '';
  }

  getLastMessageSummary(recipient: User) {
    if (!this.allMessages) {
      return '';
    }

    const recipientMessages = this.allMessages.filter((message: Message) => {
      return (message.fromId === recipient.id) || (message.toId === recipient.id);
    });

    if (recipientMessages.length > 0) {
      const lastMessage = recipientMessages[recipientMessages.length - 1];
      return lastMessage.content.substring(0, 40) + (lastMessage.content.length > 40 ? '...' : '');
    }

    return '';
  }

  changeRecipient(recipient: User) {
    this.selectedRecipient = recipient;
    this.filterMessages();
  }

  filterRecipients() {
    this.recipients = this.allRecipients.filter((user: User) => {
      return user.username.indexOf(this.searchPhrase) >= 0;
    });
  }

  private filterMessages() {
    this.messages = this.allMessages.filter((message: Message) => {
      return message.fromId === this.selectedRecipient.id || message.toId === this.selectedRecipient.id;
    });
  }

  private fetchMessages() {
    this.messageService.findMessages().subscribe((messages: Message[]) => {
      this.allMessages = messages;

      setTimeout(() => {
        this.scrollToBottom();
      }, 0);

      setTimeout(() => {
        this.markRead(messages);
      }, 3000);

      this.filterMessages();
    });
  }

  private markRead(messages: Message[]) {
    const messageIds = [];

    messages.forEach((message: Message) => {
      if (!message.read && message.toId === this.currentUser.id) {
        message.read = true;
        messageIds.push(message.id);
      }
    });

    if (messageIds.length > 0) {
      this.messageService.markRead(messageIds).subscribe();
    }
  }

  scrollToBottom(): void {
    if (this.autoScrollEnabled && this.scroll) {
      this.scroll.nativeElement.scrollTop = this.scroll.nativeElement.scrollHeight;
    }
  }

  public handleScroll(event: ScrollEvent) {
    this.autoScrollEnabled = event.isReachingBottom;
  }

  sendFriendRequest() {
    const modalReference = this.modalService.open(FriendRequestModalComponent);

    modalReference.result.then((username: string) => {
      this.friendshipService.sendRequest(username).subscribe(() => {

        this.toastr.success('Ystäväpyyntö lähetetty onnistuneesti!', '', {
          positionClass: 'toast-top-center',
          timeOut: 2000
        });
      }, () => {
        this.toastr.error('Ystäväpyynnön lähettäminen epäonnistui. Yritä hetken kuluttua uudelleen.', 'Virhe!', {
          positionClass: 'toast-top-center',
          timeOut: 3000
        });
      });
    });
  }

  acceptRequest(friendshipRequest: FriendshipRequest) {
    this.friendshipService.acceptRequest(friendshipRequest).subscribe(() => {
      this.getFriendshipRequests();
      this.getFriends();
    });
  }

  rejectRequest(friendshipRequest: FriendshipRequest) {
    this.friendshipService.rejectRequest(friendshipRequest).subscribe(() => {
      this.getFriendshipRequests();
    });
  }

  private getFriendshipRequests() {
    this.friendshipService.getRequests().subscribe((friendshipRequests: FriendshipRequest[]) => {
      this.friendshipRequests = friendshipRequests;
    });
  }

  private getFriends() {
    this.friendshipService.getFriends().subscribe((friends: User[]) => {
      this.allRecipients = friends;
      this.recipients = [...this.allRecipients];
      this.selectedRecipient = this.recipients[0];

      this.fetchMessages();

      this.newMessagesSubscription = this.messageService.newMessages.subscribe(() => {
        this.fetchMessages();
      });
    });
  }
}
