import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { UserService } from '../../services/user.service';
import { ToastrService } from 'ngx-toastr';
import { User } from '../../models/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private spinner: NgxSpinnerService,
    private usersService: UserService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.spinner.show();
    this.submitted = true;

    if (this.loginForm.invalid) {
      this.spinner.hide();
      return;
    }

    this.loading = true;

    const user: User = new User();
    user.username = this.f.username.value;
    user.password = this.f.password.value;

    this.usersService.login(user).subscribe((result: any) => {
      localStorage.setItem('fullName', result.firstName + ' ' + result.lastName);
      localStorage.setItem('token', result.token);

      this.router.navigate(['/etusivu']).then(() => {
        this.toastr.success('Kirjautuminen onnistui, tervetuloa!', '', {
          positionClass: 'toast-top-center',
          timeOut: 2000
        });
      });
    }, error => {
      this.toastr.error('Tarkista käyttäjätunnus ja salasana!', 'Kirjautuminen epäonnistui!', {
        positionClass: 'toast-top-center',
        timeOut: 3000
      });

      this.loading = false;
      this.spinner.hide();
    }, ()  => {
      this.spinner.hide();
    });
  }
}
