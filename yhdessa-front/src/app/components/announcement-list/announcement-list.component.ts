import {Component, Input, OnInit} from '@angular/core';
import {AnnouncementService} from '../../services/announcement.service';
import {Announcement} from '../../models/announcement';
import {AnnouncementType} from '../../models/announcementType';
import {Router} from '@angular/router';

@Component({
  selector: 'app-announcement-list',
  templateUrl: './announcement-list.component.html',
  styleUrls: ['./announcement-list.component.scss']
})
export class AnnouncementListComponent implements OnInit {
  @Input() title: string;
  @Input() type: AnnouncementType;
  @Input() max: number;
  @Input() headerClass: string;
  @Input() showShowAll = false;
  @Input() enableAutoLoadMore = true;

  from: number;
  announcements: Announcement[] = [];

  constructor(private announcementService: AnnouncementService, private router: Router) {
  }

  ngOnInit() {
    this.from = this.max;
    this.loadData();
  }

  showAll() {
    if (this.type === AnnouncementType.Own) {
      this.router.navigate(['/omat']);
    } else if (this.type === AnnouncementType.Events) {
      this.router.navigate(['/tapahtumat']);
    } else if (this.type === AnnouncementType.Needed) {
      this.router.navigate(['/tarvitaan']);
    } else if (this.type === AnnouncementType.Offered) {
      this.router.navigate(['/tarjotaan']);
    }
  }

  private loadData() {
    this.announcementService.getSpecificType(this.type, 0, this.max).subscribe((announcements: Announcement[]) => {
      this.announcements = announcements;
    });
  }

  open(identifier: string) {
    this.router.navigate(['/ilmoitus/', identifier]);
  }

  onScroll() {
    if (!this.enableAutoLoadMore) {
      return;
    }

    this.announcementService.getSpecificType(this.type, this.from, this.max).subscribe((announcements: Announcement[]) => {
      this.announcements = this.announcements.concat(announcements);
      this.from = this.from + this.max;
    });
  }
}
