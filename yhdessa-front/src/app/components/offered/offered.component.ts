import { Component, OnInit } from '@angular/core';
import {AnnouncementType} from '../../models/announcementType';

@Component({
  selector: 'app-offered',
  templateUrl: './offered.component.html',
  styleUrls: ['./offered.component.scss']
})
export class OfferedComponent implements OnInit {

  offered = AnnouncementType.Offered;

  constructor() { }

  ngOnInit() {
  }

}
