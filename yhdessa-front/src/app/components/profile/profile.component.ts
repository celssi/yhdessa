import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';
import {UserService} from '../../services/user.service';
import {ToastrService} from 'ngx-toastr';
import {User} from '../../models/user';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  profileForm: FormGroup;
  loading = false;
  submitted = false;
  user: User;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private spinner: NgxSpinnerService,
    private usersService: UserService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.usersService.getCurrentUser().subscribe((user: User) => {
      this.user = user;

      this.profileForm = this.formBuilder.group({
        username: [{
          value: this.user ? this.user.username : '',
          disabled: true
        }, [Validators.required, Validators.minLength(5), Validators.maxLength(15)]],
        firstName: [this.user ? this.user.firstName : '', Validators.required],
        lastName: [this.user ? this.user.lastName : '', Validators.required],
        email: [this.user ? this.user.email : '', [Validators.required, Validators.email]],
        phone: [this.user ? this.user.phone : '', Validators.pattern('^(\\+\\d{1,3}[- ]?)?\\d{10}$')],
        password: ['', [Validators.minLength(8)]],
        passwordAgain: ['']
      }, {
        validator: this.mustMatch('password', 'passwordAgain')
      });
    });
  }

  get f() { return this.profileForm.controls; }

  onSubmit() {
    this.spinner.show();
    this.submitted = true;

    if (this.profileForm.invalid) {
      this.spinner.hide();
      return;
    }

    this.loading = true;

    const user: User = new User();
    user.id = this.user.id;
    user.username = this.f.username.value;
    user.firstName = this.f.firstName.value;
    user.lastName = this.f.lastName.value;
    user.email = this.f.email.value;
    user.phone = this.f.phone.value;

    if (this.f.password.value) {
      user.password = this.f.password.value;
    }

    this.usersService.save(user).subscribe((result: any) => {
      this.f.password.setValue('');
      this.f.passwordAgain.setValue('');

      this.toastr.success('tallentaminen onnistui!', '', {
        positionClass: 'toast-top-center',
        timeOut: 2000
      });
    }, () => {
      this.toastr.error('Tarkista tiedot!', 'Rekisteröityminen epäonnistui!', {
        positionClass: 'toast-top-center',
        timeOut: 3000
      });

      this.loading = false;
      this.spinner.hide();
    }, ()  => {
      this.loading = false;
      this.spinner.hide();
    });
  }

  mustMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];

      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
        return;
      }

      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ mustMatch: true });
      } else {
        matchingControl.setErrors(null);
      }
    };
  }
}
