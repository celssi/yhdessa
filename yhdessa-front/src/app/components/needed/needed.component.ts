import { Component, OnInit } from '@angular/core';
import {AnnouncementType} from '../../models/announcementType';

@Component({
  selector: 'app-needed',
  templateUrl: './needed.component.html',
  styleUrls: ['./needed.component.scss']
})
export class NeededComponent implements OnInit {

  needed = AnnouncementType.Needed;

  constructor() { }

  ngOnInit() {
  }

}
