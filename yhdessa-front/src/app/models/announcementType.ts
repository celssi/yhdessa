export enum AnnouncementType {
    Own,
    Events,
    Needed,
    Offered
}
