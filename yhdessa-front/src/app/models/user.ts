export class User {
  id: number;
  token?: string;
  username: string;
  password?: string;
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
  active: boolean;
}
