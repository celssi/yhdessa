import {AnnouncementType} from './announcementType';
import {User} from './user';

export class Announcement {
  title: string;
  message: string;
  startDateTime: Date;
  endDateTime: Date;
  createdAt: Date;
  updatedAt: Date;
  type: AnnouncementType;
  user: User;
  accept: boolean;
  invite: boolean;
  private: boolean;
  identifier: string;
}
