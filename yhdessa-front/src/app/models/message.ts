export class Message {
  id: number;
  content: string;
  fromId: number;
  toId: number;
  createdAt: Date;
  read: boolean;
}
