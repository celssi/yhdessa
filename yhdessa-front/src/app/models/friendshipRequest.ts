import {User} from './user';

export class FriendshipRequest {
  id: number;
  user: User;
  requestee: number;
  createdAt: Date;
  updatedAt: Date;
}
