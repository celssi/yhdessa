import {Component} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {NavigationStart, Router} from '@angular/router';
import {filter} from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  hideCarousel = false;

  constructor(translate: TranslateService, private router: Router) {
    translate.setDefaultLang('fi');
    translate.use('fi');

    router.events.pipe(filter(event => event instanceof NavigationStart)).subscribe((event: NavigationStart) => {
      const {url} = event;
      this.hideCarousel = url === '/kirjaudu' || url === '/rekisteroidy';
    });
  }
}
