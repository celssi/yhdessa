import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AuthGuard } from './services/auth-guard.service';
import {FrontPageComponent} from './components/front-page/front-page.component';
import {LoginComponent} from './components/login/login.component';
import {RegisterComponent} from './components/register/register.component';
import {ProfileComponent} from './components/profile/profile.component';
import {EventsComponent} from './components/events/events.component';
import {NeededComponent} from './components/needed/needed.component';
import {OfferedComponent} from './components/offered/offered.component';
import {OwnComponent} from './components/own/own.component';
import {AnnouncementComponent} from './components/announcement/announcement.component';

const routes: Routes = [
  { path: '', redirectTo: '/etusivu', pathMatch: 'full' },
  { path: 'etusivu', component: FrontPageComponent },
  { path: 'omat', component: OwnComponent, canActivate: [AuthGuard] },
  { path: 'tapahtumat', component: EventsComponent, canActivate: [AuthGuard] },
  { path: 'tarjotaan', component: OfferedComponent, canActivate: [AuthGuard] },
  { path: 'tarvitaan', component: NeededComponent, canActivate: [AuthGuard] },
  { path: 'kirjaudu', component: LoginComponent },
  { path: 'rekisteroidy', component: RegisterComponent },
  { path: 'profiili', component: ProfileComponent, canActivate: [AuthGuard] },
  { path: 'ilmoitus/:id', component: AnnouncementComponent, canActivate: [AuthGuard] },
  { path: '**', redirectTo: '/etusivu' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
