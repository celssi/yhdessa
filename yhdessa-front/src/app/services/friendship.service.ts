import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {User} from '../models/user';
import {FriendshipRequest} from '../models/friendshipRequest';

@Injectable({
  providedIn: 'root'
})
export class FriendshipService {

  constructor(private http: HttpClient) { }

  friend(user: User) {
    return this.http.get(environment.apiUrl + 'friendships/friend/' + user.id);
  }

  unfriend(user: User) {
    return this.http.get(environment.apiUrl + 'friendships/unfriend/' + user.id);
  }

  getFriends() {
    return this.http.get(environment.apiUrl + 'friendships');
  }

  sendRequest(username: string) {
    return this.http.post(environment.apiUrl + 'friendships/request/' + username, '');
  }

  getRequests() {
    return this.http.get(environment.apiUrl + 'friendships/request');
  }

  acceptRequest(friendshipRequest: FriendshipRequest) {
    return this.http.get(environment.apiUrl + 'friendships/request/accept/' + friendshipRequest.id);
  }

  rejectRequest(friendshipRequest: FriendshipRequest) {
    return this.http.get(environment.apiUrl + 'friendships/request/reject/' + friendshipRequest.id);
  }
}
