import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {User} from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  static getToken() {
    return localStorage.getItem('token');
  }

  isLoggedIn() {
    return !!localStorage.getItem('token');
  }

  getFullName() {
    return localStorage.getItem('fullName');
  }

  login(user: User) {
    return this.http.post(environment.apiUrl + 'users/login', user);
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('fullName');
  }

  register(user: User) {
    return this.http.post(environment.apiUrl + 'users/register', user);
  }

  getCurrentUser() {
    return this.http.get(environment.apiUrl + 'users/me');
  }

  save(user: User) {
    return this.http.put(environment.apiUrl + 'users', {user});
  }

  exists(username: string) {
    return this.http.get(environment.apiUrl + 'users/exists/' + username);
  }
}
