import {EventEmitter, Injectable, OnDestroy, Output} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from '../models/user';
import {environment} from '../../environments/environment';
import {Message} from '../models/message';
import {timer} from 'rxjs';
import {UserService} from './user.service';

@Injectable({
  providedIn: 'root'
})
export class MessageService implements OnDestroy {
  private timerSubscription: any;

  @Output() newMessages: EventEmitter<number> = new EventEmitter();

  constructor(private http: HttpClient, private userService: UserService) {
    this.timerSubscription = timer(0, 5000)
      .subscribe(() => this.countNewMessages());
  }

  public ngOnDestroy(): void {
    if (this.timerSubscription) {
      this.timerSubscription.unsubscribe();
    }
  }

  findMessages() {
    return this.http.get(environment.apiUrl + 'messages');
  }

  sendMessage(user: User, message: Message) {
    return this.http.post(environment.apiUrl + 'messages/' + user.id, message);
  }

  markRead(messageIds: string[]) {
    return this.http.put(environment.apiUrl + 'messages/mark-read', messageIds);
  }

  countMessages() {
    return this.http.get(environment.apiUrl + 'messages/count');
  }

  private countNewMessages() {
    if (this.userService.isLoggedIn()) {
      this.countMessages().subscribe((count: number) => {
        this.newMessages.emit(count);
      });
    }
  }

}
