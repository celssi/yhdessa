import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Announcement} from '../models/announcement';
import {AnnouncementType} from '../models/announcementType';

@Injectable({
  providedIn: 'root'
})
export class AnnouncementService {
  constructor(private http: HttpClient) {
  }

  create(announcement: Announcement) {
    return this.http.post(environment.apiUrl + 'announcements', {announcement});
  }

  getSpecificType(type: AnnouncementType, from: number, amount: number) {
    return this.http.get(environment.apiUrl + 'announcements/' + type.toString() + '/' + from + '/' + amount);
  }

  getOne(identifier: string) {
    return this.http.get(environment.apiUrl + 'announcements/' + identifier);
  }

  attend(identifier: string) {
    return this.http.get(environment.apiUrl + 'announcements/' + identifier + '/attend');
  }

  unattend(identifier: string) {
    return this.http.get(environment.apiUrl + 'announcements/' + identifier + '/unattend');
  }

  getAttendees(identifier: string) {
    return this.http.get(environment.apiUrl + 'announcements/' + identifier + '/attendees');
  }
}
