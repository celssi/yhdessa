import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgxSpinnerModule} from 'ngx-spinner';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ToastrModule} from 'ngx-toastr';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AdsenseModule} from 'ng2-adsense';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HeaderComponent} from './components/header/header.component';
import {FooterComponent} from './components/footer/footer.component';
import {FrontPageComponent} from './components/front-page/front-page.component';
import {LoginComponent} from './components/login/login.component';
import {AuthGuard} from './services/auth-guard.service';
import {RegisterComponent} from './components/register/register.component';
import {ProfileComponent} from './components/profile/profile.component';
import {CarouselComponent} from './components/carousel/carousel.component';
import {AnnouncementModalComponent} from './components/announcement-modal/announcement-modal.component';
import {UserService} from './services/user.service';
import {AnnouncementService} from './services/announcement.service';
import {AnnouncementListComponent} from './components/announcement-list/announcement-list.component';
import {NeededComponent} from './components/needed/needed.component';
import {OwnComponent} from './components/own/own.component';
import {EventsComponent} from './components/events/events.component';
import {OfferedComponent} from './components/offered/offered.component';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import {TokenInterceptor} from './services/token.interceptor';
import { AnnouncementComponent } from './components/announcement/announcement.component';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import { ChatComponent } from './components/chat/chat.component';
import {FriendshipService} from './services/friendship.service';
import {MessageService} from './services/message.service';
import {ScrollEventModule} from 'ngx-scroll-event';
import { FriendRequestModalComponent } from './components/friend-request-modal/friend-request-modal.component';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    FrontPageComponent,
    LoginComponent,
    RegisterComponent,
    ProfileComponent,
    CarouselComponent,
    AnnouncementModalComponent,
    AnnouncementListComponent,
    NeededComponent,
    OwnComponent,
    EventsComponent,
    OfferedComponent,
    AnnouncementComponent,
    ChatComponent,
    FriendRequestModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    NgxSpinnerModule,
    HttpClientModule,
    ReactiveFormsModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule,
    NgxMaterialTimepickerModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    AdsenseModule.forRoot({
      adClient: 'ca-pub-7405552560117313'
    }),
    InfiniteScrollModule,
    FormsModule,
    ScrollEventModule
  ],
  providers: [
    AuthGuard,
    UserService,
    FriendshipService,
    AnnouncementService,
    MessageService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    AnnouncementModalComponent,
    FriendRequestModalComponent
  ]
})
export class AppModule { }
