const bcrypt = require('bcryptjs');

const db = require('../database');

const createUser = async ({ username, firstName, lastName, email, phone, password }) => {
    const salt = bcrypt.genSaltSync(10);
    password = bcrypt.hashSync(password, salt);

    return await db.User.create({ username, firstName, lastName, email, phone, password });
};

const updateUser = async (user) => {
    if (user.password) {
        const salt = bcrypt.genSaltSync(10);
        user.password = bcrypt.hashSync(user.password, salt);
    }

    return await db.User.update(
        user,
        { where: { id: user.id } }
    );
};

const getAllUsers = async () => {
    return await db.User.findAll({
        attributes: {
            exclude: ['password']
        }
    });
};

const getUser = async obj => {
    return await db.User.findOne({
        where: obj,
        attributes: {
            exclude: ['password']
        }
    });
};

const getCurrentUser = async (req) => {
    return await req.user;
};

const getUserFull = async obj => {
    return await db.User.findOne({
        where: obj
    });
};

module.exports = { createUser, updateUser, getAllUsers, getUserFull, getUser, getCurrentUser };
