const db = require('../database');
const Sequelize = require('sequelize');
const op = Sequelize.Op;

const createMessage = async (messageToCreate) => {
    const message = db.Message.build({
        content: messageToCreate.content,
        read: false
    });

    return await message.save();
};

const findMessages = async (user) => {
    return await db.Message.findAll({
        order: [['createdAt', 'ASC']],
        where: {
            [op.or]: [{fromId: user.id}, {toId: user.id}]
        }
    });
};

const findUnreadMessages = async (user) => {
    return await db.Message.findAll({
        order: [['createdAt', 'ASC']],
        where: {
            toId: user.id,
            read: false
        }
    });
};

const markRead = async (messageIds) => {
    const promises = [];
    messageIds.forEach(function(messageId){
        promises.push(db.Message.update({ read: true},{ where: { id: messageId } }));
    });

    Promise.all(promises).then(function(){
    }, function(err){
        console.log(err);
    });
};

module.exports = { createMessage, findMessages, markRead, findUnreadMessages };
