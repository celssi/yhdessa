const db = require('../database');

const friend = async (user, friendId) => {
    const friend = await db.User.findOne({
        where: {
            id: friendId
        }
    });

    user.addFriend1(friend);
    friend.addFriend1(user);
};

const unfriend = async (user, friendId) => {
    const friend = await db.User.findOne({
        where: {
            id: friendId
        }
    });

    user.removeFriend1(friend);
    friend.removeFriend1(user);
};

const makeRequest = async (user, username) => {
    const friend = await db.User.findOne({
        where: {
            username: username
        }
    });

    const request = db.FriendshipRequest.build({
        userId: user.id,
        requestee: friend.id
    });

    return await request.save();
};

const findFriends = async (user) => {
    return await user.getFriend1()
};

const findRequests = async (user) => {
    return await db.FriendshipRequest.findAll({
        order: [['createdAt', 'ASC']],
        where: {requestee: user.id, accepted: false},
        attributes: {
            exclude: ['userId']
        },
        include: [
            {
                model: db.User,
                attributes: ['username', 'id']
            }
        ]
    });
};

const acceptRequest = async (user, requestId) => {
    const request = await db.FriendshipRequest.findOne({
        where: {
            id: requestId
        }
    });

    request.accepted = true;
    await request.save();

    const friend = await request.getUser();
    user.addFriend1(friend);
    friend.addFriend1(user);
};

const rejectRequest = async (user, requestId) => {
    await db.FriendshipRequest.destroy({
        where: {
            id: requestId
        }
    });
};

module.exports = { friend, unfriend, findFriends, makeRequest, findRequests, acceptRequest, rejectRequest };
