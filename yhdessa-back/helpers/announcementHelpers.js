const db = require('../database');
const moment = require('moment');

const createAnnouncement = async (announcementToCreate) => {
    const announcement = db.Announcement.build({
        title: announcementToCreate.title,
        message: announcementToCreate.message,
        type: announcementToCreate.type,
        startDateTime: moment(announcementToCreate.startDateTime).format('YYYY-MM-DD HH:mm:ss'),
        endDateTime: moment(announcementToCreate.endDateTime).format('YYYY-MM-DD HH:mm:ss'),
        accept: announcementToCreate.accept,
        invite: announcementToCreate.invite,
        private: announcementToCreate.private
    });

    return await announcement.save();
};

const updateAnnouncement = async (announcement) => {
    return await db.Announcement.update(
        announcement,
        { where: { id: announcement.id } }
    );
};

const getAllAnnouncements = async () => {
    return await db.Announcement.findAll();
};

const getAnnouncement = async obj => {
    return await db.Announcement.findOne({
        where: obj
    });
};

const getNeeded = async (from, amount) => {
    return await db.Announcement.findAll({
        offset: from, limit: amount,
        order: [['createdAt', 'DESC']],
        where: {
            type: 2,
            private: 0
        },
        attributes: {
            exclude: ['userId']
        },
        include: [
            {
                model: db.User,
                attributes: ['username', 'id']
            }
        ]
    });
};

const getOffered = async (from, to) => {
    return await db.Announcement.findAll({
        offset: from, limit: to,
        order: [['createdAt', 'DESC']],
        where: {
            type: 3,
            private: 0
        },
        attributes: {
            exclude: ['userId']
        },
        include: [
            {
                model: db.User,
                attributes: ['username', 'id']
            }
        ]
    });
};

const getOwn = async (from, to, user) => {
    return await db.Announcement.findAll({
        offset: from, limit: to,
        order: [['createdAt', 'DESC']],
        where: {
            userId: user.id
        },
        attributes: {
            exclude: ['userId']
        },
        include: [
            {
                model: db.User,
                attributes: ['username', 'id']
            }
        ]
    });
};

const getEvents = async (from, to) => {
    return await db.Announcement.findAll({
        offset: from, limit: to,
        order: [['createdAt', 'DESC']],
        where: {
            type: 1,
            private: 0
        },
        attributes: {
            exclude: ['userId']
        },
        include: [
            {
                model: db.User,
                attributes: ['username', 'id']
            }
        ]
    });
};

const getWithIdentification = async (identifier) => {
    return await db.Announcement.findOne({
        where: {
            identifier: identifier
        },
        attributes: {
            exclude: ['userId']
        },
        include: [
            {
                model: db.User,
                attributes: ['username', 'id']
            }
        ]
    });
};

const attend = async (user, identifier) => {
    const announcement = await getAnnouncement({ identifier: identifier });

    const attendance = db.Attendance.build({
        announcementId: announcement.id,
        userId: user.id
    });

    return await attendance.save();
};

const unattend = async (user, identifier) => {
    const announcement = await getAnnouncement({ identifier: identifier });

    await db.Attendance.destroy({
        where: {
            announcementId: announcement.id,
            userId: user.id
        }
    });
};

const getAttendees = async (identifier) => {
    const announcement = await getAnnouncement({ identifier: identifier });


    return await db.Attendance.findAll({
        where: { announcementId: announcement.id },
        include: [{
            model: db.User
        }]
    });
};

module.exports = { createAnnouncement, updateAnnouncement, getAllAnnouncements, getAnnouncement, getEvents, getNeeded, getOffered, getOwn, getWithIdentification, attend, unattend, getAttendees };
