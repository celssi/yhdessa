const express = require('express');
const https = require('https');
const http = require('http');
const fs = require('fs');
const bodyParser = require('body-parser');
const helmet = require('helmet');
const passport = require('passport');
const cors = require('cors');

const auth = require('./auth');
passport.use(auth.strategy);

const app = express();

app.use(helmet());
app.use(cors());

app.use(passport.initialize(null));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(require('./routes'));

app.use(function(req, res, next) {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
});

app.use(function(err, req, res, next) {
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    res.status(err.status || 500);

    res.json({
        message: err.message,
        error: err
    });
});

const database = require('./database');

let server = http.createServer(app).listen(3000);

/*let sslOptions = {
    key: fs.readFileSync('/etc/letsencrypt/live/yhdessa.fi/privkey.pem'),
    cert: fs.readFileSync('/etc/letsencrypt/live/yhdessa.fi/cert.pem')
};

let serverHttps = https.createServer(sslOptions, app).listen(3000);*/
