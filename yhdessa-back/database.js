const Sequelize = require('sequelize');
const UserModel = require('./models/UserModel');
const AnnouncementModel = require('./models/AnnouncementModel');
const FriendshipModel = require('./models/FriendshipModel');
const FriendshipRequestModel = require('./models/FriendshipRequestModel');
const MessageModel = require('./models/MessageModel');
const AttendanceModel = require('./models/AttendanceModel');

const sequelize = new Sequelize({
    database: 'yhdessa',
    username: 'root',
    password: 'passu',
    dialect: 'mariadb',
    pool: {
        max: 10,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
    timezone: 'Europe/Helsinki'
});

sequelize
    .authenticate()
    .then(() => console.log('Connection has been established successfully.'))
    .catch(err => console.error('Unable to connect to the database:', err));

const db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.User = UserModel(sequelize, Sequelize);
db.Announcement = AnnouncementModel(sequelize, Sequelize);
db.Friendship = FriendshipModel(sequelize, Sequelize);
db.FriendshipRequest = FriendshipRequestModel(sequelize, Sequelize);
db.Message = MessageModel(sequelize, Sequelize);
db.Attendance = AttendanceModel(sequelize, Sequelize);

db.Announcement.belongsTo(db.User);
db.User.hasMany(db.Announcement);
db.User.belongsToMany(db.User, { as: 'friend1', foreignKey: 'friend2', through: db.Friendship});
db.Message.belongsTo(db.User, { as: 'from'});
db.Message.belongsTo(db.User, { as: 'to'});
db.FriendshipRequest.belongsTo(db.User);
db.User.hasMany(db.FriendshipRequest);
db.Attendance.belongsTo(db.Announcement);
db.Attendance.belongsTo(db.User);

sequelize.sync({ force: false }).then(() => {
    console.log(`Database & tables created!`)
});

module.exports = db;
