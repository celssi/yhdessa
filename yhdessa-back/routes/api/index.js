const express = require('express');
const router = express.Router();

router.use('/users', require('./users'));
router.use('/announcements', require('./announcements'));
router.use('/friendships', require('./friendships'));
router.use('/messages', require('./messages'));

module.exports = router;
