const express = require('express');
const passport = require('passport');
const announcementHelpers = require('../../helpers/announcementHelpers');
const router = express.Router();

router.get('/:type/:from/:amount', passport.authenticate('jwt', {session: false}, null), async function (req, res) {

    const type = parseInt(req.params.type);
    const from = parseInt(req.params.from);
    const amount = parseInt(req.params.amount);

    if (type === 0) {
        const user = await req.user;
        res.json(await announcementHelpers.getOwn(from, amount, user).then(announcement => res.json(announcement)));
    } else if (type === 1) {
        res.json(await announcementHelpers.getEvents(from, amount).then(announcement => res.json(announcement)));
    } else if (type === 2) {
        res.json(await announcementHelpers.getNeeded(from, amount).then(announcement => res.json(announcement)));
    } else if (type === 3) {
        res.json(await announcementHelpers.getOffered(from, amount).then(announcement => res.json(announcement)));
    }
});

router.get('/:identifier', passport.authenticate('jwt', {session: false}, null), async function (req, res) {
    res.json(await announcementHelpers.getWithIdentification(req.params.identifier));
});

router.get('/:identifier/attend', passport.authenticate('jwt', {session: false}, null), async function (req, res) {
    const currentUser = await req.user;
    announcementHelpers.attend(currentUser, req.params.identifier);

    res.json();
});

router.get('/:identifier/unattend', passport.authenticate('jwt', {session: false}, null), async function (req, res) {
    const currentUser = await req.user;
    announcementHelpers.unattend(currentUser, req.params.identifier);

    res.json();
});

router.get('/:identifier/attendees', passport.authenticate('jwt', {session: false}, null), async function (req, res) {
    const attendances = await announcementHelpers.getAttendees(req.params.identifier);

    const users = [];
    for (let i = 0; i < attendances.length; i++) {
        users.push(await attendances[i].getUser());
    }

    res.json(users);
});

router.post('', passport.authenticate('jwt', {session: false}, null), function (req, res) {
    const {announcement} = req.body;

    announcementHelpers.createAnnouncement(announcement).then(async (announcement) => {
        const user = await req.user;
        const result = await announcement.setUser(user);

        res.json(result);
    }, error => {
        res.status(400).json(error);
    });
});

module.exports = router;
