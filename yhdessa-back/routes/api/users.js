const express = require('express');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const auth = require('../../auth');
const userHelpers = require('../../helpers/userHelpers');
const bcrypt = require('bcryptjs');

const router = express.Router();

router.get('/', passport.authenticate('jwt', {session: false}, null), function (req, res) {
    userHelpers.getAllUsers().then(user => res.json(user));
});

router.get('/me', passport.authenticate('jwt', {session: false}, null), function (req, res) {
    userHelpers.getCurrentUser(req).then(user => res.json(user));
});

router.put('/', passport.authenticate('jwt', {session: false}, null), function (req, res) {
    const {user} = req.body;

    userHelpers.updateUser(user).then(() => {
            res.json({msg: 'account saved successfully'});
        }, error => {
            res.status(400).json(error);
        }
    );
});

router.post('/register', function (req, res) {
    const {username, firstName, lastName, email, phone, password} = req.body;
    userHelpers.createUser({username, firstName, lastName, email, phone, password}).then(() => {
            res.json({msg: 'account created successfully'});
        }, error => {
            res.status(400).json(error);
        }
    );
});

router.get('/exists/:username', async function (req, res) {
    const username = req.params.username;
    const user = await userHelpers.getUser({username: username});
    res.json(user !== null);
});

router.post('/login', async function (req, res) {
    const {username, password} = req.body;
    if (username && password) {
        let user = await userHelpers.getUserFull({username: username});

        if (!user) {
            res.status(401).json({message: 'No such user found'});
        } else if (!user.active) {
            res.status(403).json({message: 'User not active'});
        } else if (bcrypt.compareSync(password, user.password)) {
            let payload = {id: user.id};
            const token = jwt.sign(payload, auth.jwtOptions.secretOrKey);
            const result = {
                firstName: user.firstName,
                lastName: user.lastName,
                token: token
            };

            res.json(result);
        } else {
            res.status(401).json({msg: 'Password is incorrect'});
        }
    } else {
        res.status(400).json('password and username required');
    }
});

module.exports = router;
