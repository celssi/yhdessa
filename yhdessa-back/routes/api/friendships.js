const express = require('express');
const passport = require('passport');
const friendshipHelpers = require('../../helpers/friendshipHelpers');
const router = express.Router();

router.get('/friend/:friendId', passport.authenticate('jwt', {session: false}, null), async function (req, res) {
    const friendId = parseInt(req.params.friendId);
    const currentUser = await req.user;

    friendshipHelpers.friend(currentUser, friendId);
    res.status(200).json('successful');
});

router.get('/unfriend/:friendId', passport.authenticate('jwt', {session: false}, null), async function (req, res) {
    const friendId = parseInt(req.params.friendId);
    const currentUser = await req.user;

    friendshipHelpers.unfriend(currentUser, friendId);
    res.status(200).json('successful');
});

router.post('/request/:username', passport.authenticate('jwt', {session: false}, null), async function (req, res) {
    const username = req.params.username;
    const currentUser = await req.user;

    friendshipHelpers.makeRequest(currentUser, username);
    res.status(200).json('successful');
});

router.get('/request', passport.authenticate('jwt', {session: false}, null), async function (req, res) {
    const currentUser = await req.user;
    const requests = await friendshipHelpers.findRequests(currentUser);
    res.status(200).json(requests);
});

router.get('/request/accept/:id', passport.authenticate('jwt', {session: false}, null), async function (req, res) {
    const currentUser = await req.user;
    const requestId = req.params.id;

    await friendshipHelpers.acceptRequest(currentUser, requestId);
    res.status(200).json();
});

router.get('/request/reject/:id', passport.authenticate('jwt', {session: false}, null), async function (req, res) {
    const currentUser = await req.user;
    const requestId = req.params.id;

    await friendshipHelpers.rejectRequest(currentUser, requestId);
    res.status(200).json();
});

router.get('', passport.authenticate('jwt', {session: false}, null), async function (req, res) {
    const currentUser = await req.user;
    const friends = await friendshipHelpers.findFriends(currentUser);
    res.status(200).json(friends);
});

module.exports = router;
