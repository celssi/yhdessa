const express = require('express');
const passport = require('passport');
const messageHelpers = require('../../helpers/messageHelpers');
const userHelpers = require('../../helpers/userHelpers');
const router = express.Router();

router.get('', passport.authenticate('jwt', {session: false}, null), async function (req, res) {
    const currentUser = await req.user;
    const messages = await messageHelpers.findMessages(currentUser);
    res.status(200).json(messages);
});

router.get('/count', passport.authenticate('jwt', {session: false}, null), async function (req, res) {
    const currentUser = await req.user;
    const messages = await messageHelpers.findUnreadMessages(currentUser);
    res.status(200).json(messages.length);
});

router.post('/:to', passport.authenticate('jwt', {session: false}, null), function (req, res) {
    const message = req.body;
    const toId = parseInt(req.params.to);

    messageHelpers.createMessage(message).then(async (message) => {
        const from = await req.user;
        const to = await userHelpers.getUser({ id: toId });

        await message.setFrom(from);
        await message.setTo(to);

        res.json(message);
    }, error => {
        res.status(400).json(error);
    });
});

router.put('/mark-read', passport.authenticate('jwt', {session: false}, null), function (req, res) {
    const messageIds = req.body;
    messageHelpers.markRead(messageIds);
});

module.exports = router;
