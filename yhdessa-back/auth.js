const passportJWT = require('passport-jwt');

let ExtractJwt = passportJWT.ExtractJwt;
let JwtStrategy = passportJWT.Strategy;

let jwtOptions = {};
jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
jwtOptions.secretOrKey = 'kTYAAbqPnznB02yT6ysF';

const userHelpers = require('./helpers/userHelpers');

const strategy = new JwtStrategy(jwtOptions, async function (jwt_payload, next) {
    let user = await userHelpers.getUser({id: jwt_payload.id});

    if (user && user.active) {
        next(null, user);
    } else {
        next(null, false);
    }
});

module.exports = { strategy, jwtOptions };
