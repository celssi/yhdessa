module.exports = (sequelize, type) => {
    return sequelize.define('message', {
        content: {
            type: type.STRING(1000),
            allowNull: false,
            unique: false
        },
        read: {
            type: type.BOOLEAN,
            defaultValue: false,
            allowNull: false
        }
    })
};
