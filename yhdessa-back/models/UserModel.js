module.exports = (sequelize, type) => {
    return sequelize.define('user', {
        username: {
            type: type.STRING,
            allowNull: false,
            unique: true
        },
        firstName: {
            type: type.STRING,
            allowNull: false
        },
        lastName: {
            type: type.STRING,
            allowNull: false
        },
        email: {
            type: type.STRING,
            allowNull: false
        },
        phone: {
            type: type.STRING,
            allowNull: true
        },
        password: {
            type: type.STRING,
            allowNull: false
        },
        active: { type: type.BOOLEAN, allowNull: false, defaultValue: true }
    })
};
