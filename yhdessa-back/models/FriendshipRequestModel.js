module.exports = (sequelize, type) => {
    return sequelize.define('friendship_request', {
        accepted: {
            type: type.BOOLEAN,
            defaultValue: false,
            allowNull: false
        },
        requestee: {
            type: type.INTEGER,
            allowNull: false
        }
    })
};
