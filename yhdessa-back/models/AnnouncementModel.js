module.exports = (sequelize, type) => {
    return sequelize.define('announcement', {
        title: {
            type: type.STRING(50),
            allowNull: false,
            unique: false
        },
        message: {
            type: type.STRING(1000),
            allowNull: false,
            unique: false
        },
        startDateTime: {
            type: 'DATETIME',
            allowNull: false,
            unique: false
        },
        endDateTime: {
            type: 'DATETIME',
            allowNull: false,
            unique: false
        },
        type: {
            type: type.INTEGER,
            allowNull: false
        },
        accept: {
            type: type.BOOLEAN,
            defaultValue: false
        },
        private: {
            type: type.BOOLEAN,
            defaultValue: false
        },
        invite: {
            type: type.BOOLEAN,
            defaultValue: false
        },
        identifier: {
            type: type.UUID,
            defaultValue: type.UUIDV4
        }
    })
};
